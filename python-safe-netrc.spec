%global srcname safe-netrc
%global srcname_underscores %(echo "%{srcname}" | tr - _)
%global srcname_first_letter %(echo "%{srcname}" | cut -c1)

Name:           python-%{srcname}
Version:        1.0.0
Release:        2%{?dist}
Summary:        Safe netrc file parser

License:        GPLv2+
URL:            https://pypi.org/project/%{srcname}/
Source0:        https://files.pythonhosted.org/packages/py2.py3/%{srcname_first_letter}/%{srcname}/%{srcname_underscores}-%{version}-py2.py3-none-any.whl

BuildArch:      noarch

%global _description %{expand:
This package provides a subclass of the Python standard library netrc.netrc
class to add some custom behaviors.}

%description %_description

%package -n python2-%{srcname}
Summary:        %{summary}
BuildRequires:  python2-devel
BuildRequires:  python2-pip
BuildRequires:  python2-pytest
BuildRequires:  python2-wheel
%{?python_provide:%python_provide python2-%{srcname}}
%description -n python2-%{srcname} %_description

%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{summary}
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-pip
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  python%{python3_pkgversion}-wheel
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
%description -n python%{python3_pkgversion}-%{srcname} %_description

%prep
%__mkdir_p dist
%__cp %{S:0} dist

%install
%py2_install_wheel %{basename:%{S:0}}
%py3_install_wheel %{basename:%{S:0}}

%check
PYTHONPATH=%{buildroot}%{python2_sitelib} %{__python2} -B -m pytest --pyargs %{srcname_underscores}
PYTHONPATH=%{buildroot}%{python3_sitelib} %{__python3} -B -m pytest --pyargs %{srcname_underscores}

# Note that there is no %%files section for the unversioned python module
%files -n python2-%{srcname}
%{python2_sitelib}

%files -n python%{python3_pkgversion}-%{srcname}
%{python3_sitelib}

%changelog
* Tue Feb 11 2020 Leo Singer <leo.singer@ligo.org> 1.0.0-2
- Don't override Python bytecode when running unit tests.

* Mon Feb 10 2020 Leo Singer <leo.singer@ligo.org> 1.0.0-1
- New upstream release
- Run unit tests

* Sun Feb 9 2020 Leo Singer <leo.singer@ligo.org> 0.0.1-2
- Use python3_pkgversion

* Fri Feb 7 2020 Leo Singer <leo.singer@ligo.org> 0.0.1-1
- Initial RPM release
