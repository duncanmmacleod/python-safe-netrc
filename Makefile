PACKAGE=$(shell echo *.spec | sed -e 's/.spec$$//')

sources:
	spectool -g $(PACKAGE).spec

srpm:
	rpmbuild -D '_srcrpmdir ./' -D '_sourcedir ./' -bs $(PACKAGE).spec
